package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.image.ImageRaster;

/**
 * test
 *
 * @author normenhansen
 */
public class Main extends SimpleApplication {

    Material mat_terrain;
    private int maxy = 5;

    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        Box b = new Box(1, 1, 1);
        Geometry geom = new Geometry("Box", b);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        geom.setMaterial(mat);

        mat_terrain = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");
        mat_terrain.setTexture("Alpha", assetManager.loadTexture("Textures/alphamap.png"));

        /**
         * Add grid texture into the blue layer (Tex3)
         */
        Texture surfaceTexture = assetManager.loadTexture("Textures/grid8.png");
        surfaceTexture.setWrap(Texture.WrapMode.Repeat);
        mat_terrain.setTexture("Tex3", surfaceTexture);
        mat_terrain.setFloat("Tex3Scale", 4f);

        Texture theMap;
        theMap = assetManager.loadTexture("Textures/play.png");

        Image colorImage = theMap.getImage();

        int imageWidth = colorImage.getWidth();
        int imageHeight = colorImage.getHeight();

        if (imageWidth % 3 != 0 || imageHeight % 3 != 0) {
            throw new RuntimeException("imageWidth: " + imageWidth + " or imageHeight: " + imageHeight + " not divisible by 3");
        }

        System.out.println("number of thingys " + imageHeight + "  " + imageWidth * imageHeight);
        ImageRaster raster = ImageRaster.create(colorImage);

        System.out.println(this.getColourAt(0, 0, raster) + "," + this.getColourAt(1, 0, raster) + "," + this.getColourAt(2, 0, raster));
        System.out.println(this.getColourAt(0, 1, raster) + "," + this.getColourAt(1, 1, raster) + "," + this.getColourAt(2, 1, raster));
        System.out.println(this.getColourAt(0, 2, raster) + "," + this.getColourAt(1, 2, raster) + "," + this.getColourAt(2, 2, raster));
        System.out.println("---------------------------------------");

        for (int w0 = 0; w0 < imageWidth; w0 += 3) {
            int w1 = w0 + 1;
            int w2 = w0 + 2;

            for (int h0 = 0; h0 < imageHeight; h0 += 3) {
                int h1 = h0 + 1;
                int h2 = h0 + 2;

                System.out.println(this.getColourAt(w0, h0, raster) + "," + this.getColourAt(w1, h0, raster) + "," + this.getColourAt(w2, h0, raster));
                System.out.println(this.getColourAt(w0, h1, raster) + "," + this.getColourAt(w1, h1, raster) + "," + this.getColourAt(w2, h1, raster));
                System.out.println(this.getColourAt(w0, h2, raster) + "," + this.getColourAt(w1, h2, raster) + "," + this.getColourAt(w2, h2, raster));

                Node tempNode = new Node(w0 + "," + h0);

                float[][] colourbl = new float[2][2];
                float[][] colourbr = new float[2][2];
                float[][] colourtr = new float[2][2];
                float[][] colourtl = new float[2][2];

                colourbl[0][0] = this.getColourAt(w0, h0, raster);
                colourbl[1][0] = this.getColourAt(w1, h0, raster);
                colourbl[1][1] = this.getColourAt(w1, h1, raster);
                colourbl[0][1] = this.getColourAt(w0, h1, raster);

                colourbr[0][0] = this.getColourAt(w1, h0, raster);
                colourbr[1][0] = this.getColourAt(w2, h0, raster);
                colourbr[1][1] = this.getColourAt(w2, h1, raster);
                colourbr[0][1] = this.getColourAt(w1, h1, raster);

                colourtr[0][0] = this.getColourAt(w1, h1, raster);
                colourtr[1][0] = this.getColourAt(w2, h1, raster);
                colourtr[1][1] = this.getColourAt(w2, h2, raster);
                colourtr[0][1] = this.getColourAt(w1, h2, raster);

                colourtl[0][0] = this.getColourAt(w0, h1, raster);
                colourtl[1][0] = this.getColourAt(w1, h1, raster);
                colourtl[1][1] = this.getColourAt(w1, h2, raster);
                colourtl[0][1] = this.getColourAt(w0, h2, raster);

                printColourArray("Colour BL", colourbl);
                printColourArray("Colour BR", colourbr);
                printColourArray("Colour TR", colourtr);
                printColourArray("Colour TL", colourtl);

                /* make a mesh and stick it in a geometry, and rotate it properly */
                Quad topleft = new Quad(1, 1);
                Quad topright = new Quad(1, 1);
                Quad bottomright = new Quad(1, 1);
                Quad bottomleft = new Quad(1, 1);

                setQuadBuffer(topleft, colourtl);
                setQuadBuffer(topright, colourtr);
                setQuadBuffer(bottomleft, colourbl);
                setQuadBuffer(bottomright, colourbr);

                Geometry geotl = new Geometry("topleft", topleft);
                Geometry geotr = new Geometry("topright", topright);
                Geometry geobr = new Geometry("bottomright", bottomright);
                Geometry geobl = new Geometry("bottomleft", bottomleft);

                geotl.setLocalTranslation(0, 0, 0);
                geotr.setLocalTranslation(1, 0, 0);
                geobr.setLocalTranslation(1, 0, 1);
                geobl.setLocalTranslation(0, 0, 1);

                geotl.setMaterial(mat_terrain);
                geotr.setMaterial(mat_terrain);
                geobl.setMaterial(mat_terrain);
                geobr.setMaterial(mat_terrain);

                tempNode.attachChild(geotl);
                tempNode.attachChild(geotr);
                tempNode.attachChild(geobr);
                tempNode.attachChild(geobl);

                //tempNode.scale(100, 0, 100);
                tempNode.setLocalTranslation(new Vector3f((w0 / 3) * 2, 0, (h0 / 3) * 2));

                rootNode.attachChild(tempNode);
            }

        }
    }

    private Float getColourAt(int x, int y, ImageRaster r) {
        if (x < 0 || y < 0 || x >= r.getWidth() || y >= r.getHeight()) {
            return null;
        }

        float blue = r.getPixel(x, y).getBlue();
        float red = r.getPixel(x, y).getRed();
        float green = r.getPixel(x, y).getGreen();

        return ((blue + red + green) / 3) * maxy;
    }

    private Void printColourArray(String name, float[][] colours) {
        System.out.println(name);

        System.out.println(colours[0][0] + "," + colours[1][0] + "," + colours[1][1] + "," + colours[0][1]);

        return null;
    }

    private Void setQuadBuffer(Quad aquad, float[][] colours) {
        aquad.setBuffer(VertexBuffer.Type.Position, 3, new float[]{
            0, colours[0][1], 0,
            1, colours[1][1], 0,
            1, colours[1][0], 1,
            0, colours[0][0], 1
        });

        return null;
    }
}
